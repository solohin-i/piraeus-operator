REPO?=solohin/piraeus-operator:master_111019_v2

update-image:
	operator-sdk build $(REPO)
	docker push $(REPO)

helm-install:
	helm install --name=my-piraeus --namespace=git2prod-system ./piraeus-helm-chart --set image.tag=master_111019_v2

helm-upgrade:
	helm upgrade my-piraeus --namespace=git2prod-system ./piraeus-helm-chart --set image.tag=master_111019_v2,etcdURL=etcd-piraeus:2379


helm-delete: 
	helm delete my-piraeus --purge

CLEANUP:
	helm del --purge my-piraeus; true
	
	kubectl delete piraeuscontrollersets --all-namespaces --all --wait=false
	kubectl delete piraeusnodesets --all-namespaces --all --force --wait=false
	kubectl delete crd piraeuscontrollersets.piraeus.linbit.com; true
	kubectl delete crd piraeusnodesets.piraeus.linbit.com; true

	sleep 20

	kubectl --namespace=git2prod-system patch piraeusnodesets/hello-piraeus -p '{"metadata":{"finalizers":[]}}' --type=merge  ; true
	kubectl --namespace=git2prod-system patch piraeuscontrollersets/hello-piraeus -p '{"metadata":{"finalizers":[]}}' --type=merge  ; true

	kubectl patch crd/piraeusnodesets.piraeus.linbit.com -p '{"metadata":{"finalizers":[]}}' --type=merge  ; true
	kubectl patch crd/piraeuscontrollersets.piraeus.linbit.com -p '{"metadata":{"finalizers":[]}}' --type=merge ; true
	
	kubectl --namespace=git2prod-system delete daemonset --all
	kubectl --namespace=git2prod-system delete statefulset --all

sync: 
	git remote set-url origin git@gitlab.com:solohin-i/piraeus-operator.git
	git pull
	git remote set-url origin git@gitlab.com:linbit/piraeus-operator.git
	git pull
	git remote set-url origin git@gitlab.com:solohin-i/piraeus-operator.git
	git push